package rmi_tester.hello;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Date;

import rmi_tester.Env;

public interface HelloServer extends Remote {

	public String getHello() throws RemoteException;

	public static class Main extends UnicastRemoteObject implements HelloServer {

		private static final long serialVersionUID = -6344792611387958970L;

		protected Main() throws RemoteException {
			super();
		}

		public String getHello() throws RemoteException {
			System.out.println("Client connection at " + new Date().toString());
			return "안녕하세요! 반갑습니다.";
		}

		public static void main(String[] args) throws RemoteException, MalformedURLException {
			Naming.rebind(Env.SERVICE_MAIN, new Main());
			System.out.println("RMI service [rmi://" + Env.HOSTNAME + "/" + Env.SERVICE_MAIN + "] is ready...");
		}
	}
}
