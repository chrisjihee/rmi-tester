package rmi_tester;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import rmi_tester.hello.HelloServer;

public class Demo {
	public static void main(String[] args) throws MalformedURLException, RemoteException, NotBoundException {
		testHello();
		System.out.println("Test RMI [OK]");
	}

	public static void testHello() throws MalformedURLException, RemoteException, NotBoundException {
		HelloServer hello = (HelloServer) Naming.lookup("rmi://" + Env.HOSTNAME + "/" + Env.SERVICE_MAIN);
		System.out.println("RMI Result : " + hello.getHello());
	}
}
